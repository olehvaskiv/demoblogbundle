<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\BlogBundle\Entity\BlogSettingsTranslation;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\SeoBundle\Form\SeoTypeTrait;
    
class BlogSettingsTranslationType extends AbstractType {
    
    use SeoTypeTrait;
    
    public function buildForm(FormBuilderInterface $builder, array $options): void {
        
        $this->buildSeoForm($builder, $options);
        
        $builder->add('title', null, [
            'label' => 'Title',
        ]);
        
        $builder->add('content', null, [
            'label' => 'Content',
        ]);
        
        $builder->add('alt', null, [
            'label' => 'Alternative text',
        ]);
        
        $builder->add('locale', HiddenType::class, []);
    }
    
    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => BlogSettingsTranslation::class,
        ]);
    }
}

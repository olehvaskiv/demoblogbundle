<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\BlogBundle\Entity\BlogSettings;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\BlogBundle\Form\BlogSettingsTranslationType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    
class BlogSettingsType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void {
        
        $builder->add('translations', CollectionType::class, [
            'entry_type' => BlogSettingsTranslationType::class
        ]);
        
        $builder->add('contentPosition', ChoiceType::class, [
            'label' => 'Content position',
            'choices' => [
                'Top' => 'top',
                'Bottom' => 'bottom',
                'Hidden' => 'hidden',
            ],
            'expanded' => true,
            'multiple' => false
        ]);
        
        $builder->add('itemsLimit', null, [
            'label' => 'Items per page'
        ]);
                
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('photo', 'full');
            
            if ($object->getId() == null ) {
                $form->add('file', FileType::class, [
                    'label' => 'New image',
                    'constraints' => [
                        new Image([
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height']
                        ]),
                        new NotBlank([
                            'message' => 'Complete the field'
                        ])
                    ]
                ]);
            } else {
                $form->add('file', FileType::class, [
                    'label' => 'New image',
                    'constraints' => [
                        new Image([
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height']
                        ])
                    ]
                ]);
            }
        });
    }
    
    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => BlogSettings::class,
        ]);
    }
}

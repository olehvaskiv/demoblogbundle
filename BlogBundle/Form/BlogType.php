<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\BlogBundle\Entity\Blog;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\BlogBundle\Form\BlogTranslationType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use App\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\GalleryBundle\Form\HasGalleryType;
    
class BlogType extends AbstractType {
    
    use HasGalleryType;
    
    public function buildForm(FormBuilderInterface $builder, array $options): void {
        
        $this->buildGalleryForm($builder, $options); //gallery field
        
        $builder->add('translations', CollectionType::class, [
            'entry_type' => BlogTranslationType::class
        ]);
        
        $builder->add('published', null, [
            'label' => 'Published'
        ]);
        
        $builder->add('publishedAt', TextType::class, [
            'label' => 'Publication date',
        ]);
        $builder->get('publishedAt')->addModelTransformer(new DateTimeTransformer());
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('photo', 'listBig');
            
            if ($object->getId() == null ) {
                $form->add('file', FileType::class, [
                    'label' => 'New image',
                    'constraints' => [
                        new Image([
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height']
                        ]),
                        new NotBlank([
                            'message' => 'Complete the field'
                        ])
                    ]
                ]);
            } else {
                $form->add('file', FileType::class, [
                    'label' => 'New image',
                    'constraints' => [
                        new Image([
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height']
                        ])
                    ]
                ]);
            }
        });
    }
    
    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}

<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\EventListener;
    
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use App\BlogBundle\Entity\Blog;
use App\BlogBundle\Entity\BlogSettings;
use App\CoreBundle\Traits\SeoTrait;
use App\MenuBundle\Entity\MenuItem;
use App\MenuBundle\Entity\MenuItemRouteParameters;

class BlogUpdater implements EventSubscriber {
    
    use SeoTrait;
    
    public function getSubscribedEvents() {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postUpdate,
            Events::preRemove,
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getObject();

        if ($entity instanceof Blog) {
            $entity = $this->generateSeo($entity);
        }

        if ($entity instanceof BlogSettings) {
            $entity = $this->generateSeo($entity);
        }
    }
    
    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getObject();

        if ($entity instanceof Blog) {
            $entity = $this->generateSeo($entity);
        }

        if ($entity instanceof BlogSettings) {
            $entity = $this->generateSeo($entity);
        }
    }
    
    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getObject();
        $em = $args->getObjectManager();

        if ($entity instanceof Blog) {
            //update page slug in menu
            $menuRouteParameters = $em->getRepository(MenuItemRouteParameters::class)->findBy(['entityName' => Blog::class, 'entityId' => $entity->getId()]);
            foreach ($menuRouteParameters as $routeParameter) {
                $routeParameter->setValue($entity->getTranslation($routeParameter->getLocale())->getSlug());
                $em->persist($routeParameter);
                $em->flush();
            }
            //end update page slug in menu
        }
    }
    
    public function preRemove(LifecycleEventArgs $args) {
        $entity = $args->getObject();
        $em = $args->getObjectManager();

        if ($entity instanceof Blog) {
            $entity->deletePhoto('photo');
            
            //remove page from menu
            $menuItems = $em->createQueryBuilder()
                ->select('m')
                ->from(MenuItem::class, 'm')
                ->leftJoin('m.routeParameters', 'rp')
                ->where('rp.entityName = :entityName')
                ->andWhere('rp.entityId = :entityId')
                ->setParameter('entityName', Blog::class)
                ->setParameter('entityId', $entity->getId())
                ->getQuery()
                ->getResult();
            
            if (count($menuItems) > 0) {
                foreach ($menuItems as $menuItem) {
                    $em->remove($menuItem);
                    $em->flush();
                }
            }
            //end remove page from menu
        }
    }
}

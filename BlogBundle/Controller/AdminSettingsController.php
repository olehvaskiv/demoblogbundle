<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use App\BlogBundle\Entity\BlogSettings;
use App\BlogBundle\Entity\BlogSettingsTranslation;
use App\BlogBundle\Form\BlogSettingsType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\CoreBundle\Traits\AdminTrait;
use App\LogsBundle\Controller\LogsTrait;
use App\CoreBundle\Traits\SeoTrait;

/**
 * @Route("/admin/blog-settings")
 */
class AdminSettingsController extends AbstractController {
    
    use AdminTrait;
    use LogsTrait;
    use SeoTrait;
    
    /**
     * @Route("/", name="admin_blog_settings")
     */
    public function index(Request $request, Breadcrumbs $breadcrumbs): Response {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository(BlogSettings::class)->findOneBy([]);
        
        if (!$entity) {
            $entity = new BlogSettings();
        }
        
        $size = $entity->getThumbSize('photo', 'full');

        $locales = $this->getParameter('front_locales');
        foreach ($locales as $locale) {
            if ($entity->getTranslation($locale) === null) {
                $translation = new BlogSettingsTranslation();
                $translation->setLocale($locale);
                $entity->addTranslation($translation);
            }
        }
        
        $form = $this->createForm(BlogSettingsType::class, $entity);
        $form->add('submit', SubmitType::class, ['label' => 'Save']);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setUpdatedAt(new \DateTime());
            $entity = $this->generateSeo($entity);
            
            $em->persist($entity);
            $em->flush();
            
            $this->createAdminLog($request, "Edit", "Blog settings", $entity->toString($this->getParameter('default_front_locale')));
            
            $this->addFlash('success', 'The update was completed successfully');
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirectToRoute('admin_blog_settings');
            }
        }
        
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Check the form fields.');
        }
        
        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));
        $breadcrumbs->addItem("Settings", $this->generateUrl("admin_blog_settings"));
        $breadcrumbs->addItem($entity->toString($this->getParameter('default_front_locale')), $this->generateUrl("admin_blog_settings"));
        
        return $this->render('@Blog/AdminSettings/form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'size' => $size
        ]);
    }
    
    /**
     * @Route("/crop/{id<\d+>}", name="admin_blog_settings_crop")
     */
    public function crop(Request $request, Breadcrumbs $breadcrumbs, BlogSettings $entity) : ?Response {
        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));
        $breadcrumbs->addItem("Settings", $this->generateUrl("admin_blog_settings"));
        $breadcrumbs->addItem("Cropping", $this->generateUrl("admin_blog_settings_crop", ['id' => $entity->getId()]));
        
        return $this->cropFunction($request, $entity, 'admin_blog_settings_crop_save', 'admin_blog_settings', ['id' => $entity->getId()]);
    }

    /**
     * @Route("/crop/{id<\d+>}/save", name="admin_blog_settings_crop_save")
     */
    public function cropSave(Request $request, BlogSettings $entity) : ?Response {
        $this->createAdminLog($request, "Cropping", "Blog settings", $entity->toString($this->getParameter('default_front_locale')));
        
        return $this->cropSaveFunction($request, $entity, 'admin_blog_settings', []);
    }
}

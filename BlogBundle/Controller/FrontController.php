<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\BlogBundle\Repository\BlogRepository;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use App\BlogBundle\Entity\BlogSettings;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/blog")
 */
class FrontController extends AbstractController {    
    
    /**
     * @Route("/{page<\d+>}", name="blog")
     */
    public function index(Request $request, PaginatorInterface $paginator, BlogRepository $repository, Breadcrumbs $breadcrumbs, int $page = 1): Response {
        $locale = $request->getLocale();
        
        $em = $this->getDoctrine()->getManager();
        $pageSettings = $em->getRepository(BlogSettings::class)->findOneBy([]);
        if (!$pageSettings) {
            throw $this->createNotFoundException('Page not found');
        }
        
        $limit = $pageSettings->getItemsLimit();
        $entities = $paginator->paginate(
            $repository->findAllSuitable($locale),
            $page,
            $limit
        );
        $entities->setTemplate('@Core/Front/paginator.html.twig');
        
        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('blog'));
        }
        
        if ($pageSettings->getTranslation($locale) != null) {
            $breadcrumbs->addItem($pageSettings->getTranslation($locale)->getTitle(), $this->generateUrl("blog", [], UrlGeneratorInterface::ABSOLUTE_URL));
        }
        
        return $this->render('@Blog/Front/index.html.twig', [
            'entities' => $entities,
            'pageSettings' => $pageSettings,
        ]);
    }
    
    /**
     * @Route("/{slug}", name="blog_show")
     */
    public function show(Request $request, BlogRepository $repository, Breadcrumbs $breadcrumbs, $slug): Response {
        $em = $this->getDoctrine()->getManager();
        $pageSettings = $em->getRepository(BlogSettings::class)->findOneBy([]);
        if (!$pageSettings) {
            throw $this->createNotFoundException('Page not found');
        }
        
        $locale = $request->getLocale();
        $entity = $repository->findOneBySlug($slug, $locale);

        if (!$entity) {
            throw $this->createNotFoundException('The Blog entity does not exist');
        }
        
        $routeParameters = [];
        foreach ($entity->getTranslations() as $translation) {
            $routeParameters[$translation->getLocale()] = [
                'slug' => $translation->getSlug()
            ];
        }
        $request->attributes->set('locale_switcher_route_parameters', $routeParameters);
        
        $breadcrumbs->addItem($pageSettings->getTranslation($locale)->getTitle(), $this->generateUrl("blog", [], UrlGeneratorInterface::ABSOLUTE_URL));
        $breadcrumbs->addItem($entity->getTranslation($locale)->getTitle(), $this->generateUrl("blog_show", ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_URL));
        
        return $this->render('@Blog/Front/show.html.twig', [
            'entity' => $entity,
            'pageSettings' => $pageSettings,
        ]);
    }
    
    public function section(Request $request, BlogRepository $repository) {
        $locale = $request->getLocale();
        $entities = $repository->findAllSuitable($locale, 4);
        
        return $this->render('@Blog/Front/section.html.twig', [
            'entities' => $entities
        ]);
    }
}
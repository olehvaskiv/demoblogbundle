<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use App\BlogBundle\Entity\Blog;
use App\BlogBundle\Entity\BlogTranslation;
use App\BlogBundle\Form\BlogType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\CoreBundle\Traits\AdminTrait;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\LogsBundle\Controller\LogsTrait;
use App\CoreBundle\Traits\SeoTrait;

/**
 * @Route("/admin/blog")
 */
class AdminController extends AbstractController {
    
    use AdminTrait;
    use LogsTrait;
    use SeoTrait;
    
    /**
     * @Route("/", name="admin_blog")
     */
    public function index(Request $request, PaginatorInterface $paginator, Breadcrumbs $breadcrumbs): Response {
        $limit = $request->query->getInt('limit', 20);
        $page = $request->query->getInt('page', 1);
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from(Blog::class, 'e')
            ->getQuery();

        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            [
                'defaultSortFieldName' => 'e.id',
                'defaultSortDirection' => 'desc',
            ]
        );
        $entities->setTemplate('@Core/Admin/paginator.html.twig');
        
        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('admin_blog'));
        }

        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));

        return $this->render('@Blog/Admin/index.html.twig', [
            'entities' => $entities,
        ]);
    }
    
    /**
     * @Route("/new", name="admin_blog_new")
     */
    public function new(Request $request, Breadcrumbs $breadcrumbs): Response {
        $entity = new Blog();
        $size = $entity->getThumbSize('photo', 'listBig');

        $locales = $this->getParameter('front_locales');
        foreach ($locales as $locale) {
            $translation = new BlogTranslation();
            $translation->setLocale($locale);
            $entity->addTranslation($translation);
        }
        
        $form = $this->createForm(BlogType::class, $entity);
        $form->add('submit', SubmitType::class, ['label' => 'Save']);
        $form->add('submit_and_list', SubmitType::class, ['label' => 'Save and back to list']);
        $form->add('submit_and_new', SubmitType::class, ['label' => 'Save and add the next']);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUpdatedAt(new \DateTime());
            
            $entity = $this->generateSeo($entity);
            
            $em->persist($entity);
            $em->flush();
            
            $this->createAdminLog($request, "Create", "Blog", $entity->toString($this->getParameter('default_front_locale')));
            
            $this->addFlash('success', 'Adding successful');
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirectToRoute('admin_blog_edit', ['id' => $entity->getId()]);
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirectToRoute('admin_blog');
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirectToRoute('admin_blog_new');
            }
        }
        
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Check the form fields.');
        }
        
        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));
        $breadcrumbs->addItem($entity->toString($this->getParameter('default_front_locale')), $this->generateUrl("admin_blog_new"));
        
        return $this->render('@Blog/Admin/form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'size' => $size
        ]);
    }
    
    /**
     * @Route("/edit/{id<\d+>}", name="admin_blog_edit")
     */
    public function edit(Request $request, Breadcrumbs $breadcrumbs, Blog $entity): Response {
        $locales = $this->getParameter('front_locales');
        foreach ($locales as $locale) {
            if ($entity->getTranslation($locale) === null) {
                $translation = new BlogTranslation();
                $translation->setLocale($locale);
                $entity->addTranslation($translation);
            }
        }
        
        $size = $entity->getThumbSize('photo', 'listBig');
        
        $form = $this->createForm(BlogType::class, $entity);
        $form->add('submit', SubmitType::class, ['label' => 'Save']);
        $form->add('submit_and_list', SubmitType::class, ['label' => 'Save and back to list']);
        $form->add('submit_and_new', SubmitType::class, ['label' => 'Save and add the next']);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUpdatedAt(new \DateTime());
            $entity = $this->generateSeo($entity);
            
            $em->persist($entity);
            $em->flush();
            
            $this->createAdminLog($request, "Edit", "Blog", $entity->toString($this->getParameter('default_front_locale')));
            
            $this->addFlash('success', 'The update was completed successfully');
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirectToRoute('admin_blog_edit', ['id' => $entity->getId()]);
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirectToRoute('admin_blog');
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirectToRoute('admin_blog_new');
            }
        }
        
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Check the form fields.');
        }
        
        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));
        $breadcrumbs->addItem($entity->toString($this->getParameter('default_front_locale')), $this->generateUrl("admin_blog_new"));
        
        return $this->render('@Blog/Admin/form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'size' => $size
        ]);
    }
    
    /**
     * @Route("/delete/{id<\d+>}", name="admin_blog_delete")
     */
    public function delete(Request $request, Blog $entity) : Response {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        $this->createAdminLog($request, "Remove", "Blog", $entity->toString($this->getParameter('default_front_locale')));
        
        $this->addFlash('success', 'Removal completed successfully.');
        
        return new Response('OK');
    }
    
    /**
     * @Route("/selected", name="admin_blog_selected")
     */
    public function selected(Request $request, Breadcrumbs $breadcrumbs) : Response {
        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));
        $breadcrumbs->addItem("Selected", $this->generateUrl("admin_blog_selected"));
        
        return $this->selectedAction($request, 'admin_blog');
    }
    
    /**
     * @Route("/selected/execute", name="admin_blog_selected_execute")
     */
    public function selectedExecute(Request $request, TranslatorInterface $translator) : Response {
        return $this->selectExecuteAction($request, $translator, "Blog", Blog::class, 'admin_blog');
    }
    
    /**
     * @Route("/crop/{id<\d+>}", name="admin_blog_crop")
     */
    public function crop(Request $request, Breadcrumbs $breadcrumbs, Blog $entity) : ?Response {
        $breadcrumbs->addItem("Dashboard", $this->generateUrl("admin"));
        $breadcrumbs->addItem("Blog", $this->generateUrl("admin_blog"));
        $breadcrumbs->addItem("Cropping", $this->generateUrl("admin_blog_crop", ['id' => $entity->getId()]));
        
        return $this->cropFunction($request, $entity, 'admin_blog_crop_save', 'admin_blog_edit', ['id' => $entity->getId()]);
    }

    /**
     * @Route("/crop/{id<\d+>}/save", name="admin_blog_crop_save")
     */
    public function cropSave(Request $request, Blog $entity) : ?Response {
        $this->createAdminLog($request, "Cropping", "Blog", $entity->toString($this->getParameter('default_front_locale')));
        
        return $this->cropSaveFunction($request, $entity, 'admin_blog_edit', ['id' => $entity->getId()]);
    }
}

<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Entity;
    
use Doctrine\ORM\Mapping as ORM;
use App\BlogBundle\Entity\BlogTranslation;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\CoreBundle\Traits\ImagesEntityTrait;
use App\GalleryBundle\Entity\HasGallery;

/**
 * @ORM\Entity(repositoryClass="App\BlogBundle\Repository\BlogRepository")
 * @ORM\Table(name="blog")
 */
class Blog {
    
    use ImagesEntityTrait;
    use HasGallery;
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var \DateTime
     */
    private $published;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $publishedAt;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="BlogTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;
    
    //for SEO generate
    public $getTitleFunction = "getTitle";
    public $getContentFunctions = ['getContent'];
    
    protected $file;

    public function __construct() {
        $this->translations = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->publishedAt = new \DateTime();
        $this->published = true;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getPhotosParameters() {
        return [
            'photo' => [
                'uploadedFile' => $this->file,
                'filename' => $this->photo,
                'filenameSetter' => 'setPhoto',
                'convertableToWebp' => true,
                'thumbs' => [
                    'listBig' => [
                        'width' => 750,
                        'height' => 500
                    ],
                    'listMedium' => [
                        'width' => 750,
                        'height' => 250
                    ],
                    'listSmall' => [
                        'width' => 375,
                        'height' => 250
                    ]
                ]
            ]
        ];
    }
    
    protected function getUploadDir() {
        return 'uploads/blog';
    }
    
    public function setPublished(bool $published) : self
    {
        $this->published = $published;

        return $this;
    }

    public function getPublished() : ?bool
    {
        return $this->published;
    }
    
    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt() : ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt) : self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getPublishedAt() : ?\DateTime
    {
        return $this->publishedAt;
    }
    
    public function getTranslation(string $locale) {
        $translation = null;
        foreach ($this->translations as $item) {
            if (strcmp($item->getLocale(), $locale) == 0) {
                $translation = $item;
            }
        }
        return $translation;
    }

    public function getTranslations() {
        return $this->translations;
    }

    public function addTranslation(BlogTranslation $translation) : self {
        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
            $translation->setObject($this);
        }

        return $this;
    }

    public function removeTranslation(BlogTranslation $translation) : self {
        $this->translations->removeElement($translation);
        
        return $this;
    }
    
    public function toString($locale = 'en') : string {
        if (is_null($this->getTranslation($locale))) {
            return 'New';
        } else {
            if (is_null($this->getTranslation($locale)->getTitle())) {
                return 'New';
            }
            return $this->getTranslation($locale)->getTitle();
        }
    }
    
    public function setPhoto(string $photo) : self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getPhoto() : ?string
    {
        return $this->photo;
    }
    
    public function setFile(UploadedFile $file = null) : void
    {
        $this->file = $file;
    }

    public function getFile() : ?UploadedFile
    {
        return $this->file;
    }
}
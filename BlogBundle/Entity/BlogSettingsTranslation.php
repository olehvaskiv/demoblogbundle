<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */    
    
namespace App\BlogBundle\Entity;
    
use Doctrine\ORM\Mapping as ORM;
use App\BlogBundle\Entity\BlogSettings;
use App\SeoBundle\Entity\SeoEntityTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="blog_settings_translation")
 */
class BlogSettingsTranslation {
    
    use SeoEntityTrait;
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $alt;
    
    /**
     * @ORM\Column(type="string", length=8)
     * @var string
     */
    private $locale;
    
    /**
     * @ORM\ManyToOne(
     *     targetEntity="BlogSettings",
     *     inversedBy="translations"
     * )
     * @ORM\JoinColumn(
     *    name="object_id",
     *    referencedColumnName="id",
     *    onDelete="CASCADE"
     * )
     */
    private $object;
    
    private $slug;
    public $noSlug; //hide slug field
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    public function setTitle(string $title = null): self
    {
        $this->title = $title;
        
        return $this;
    }
    
    public function getContent(): ?string
    {
        return $this->content;
    }
    
    public function setContent(string $content = null): self
    {
        $this->content = $content;
        
        return $this;
    }
    
    public function setAlt(string $alt = null) : self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getAlt() : ?string
    {
        return $this->alt;
    }
    
    public function setLocale(string $locale) : self {
        $this->locale = $locale;

        return $this;
    }

    public function getLocale() : ?string {
        return $this->locale;
    }
    
    public function setObject(BlogSettings $object = null) : self {
        $this->object = $object;

        return $this;
    }
    
    public function getObject() {
        return $this->object;
    }
    
    /*
     * for checking access to inline editor
     */
    public function getEditRoute() {
        return 'admin_blog_settings';
    }
}
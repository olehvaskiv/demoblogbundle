<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Entity;
    
use Doctrine\ORM\Mapping as ORM;
use App\BlogBundle\Entity\BlogSettingsTranslation;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\CoreBundle\Traits\ImagesEntityTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="blog_settings")
 */
class BlogSettings {
    
    use ImagesEntityTrait;
    
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer", length=11)
     * @var string
     */
    private $itemsLimit;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $contentPosition;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="BlogSettingsTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;
    
    //for SEO generate
    public $getTitleFunction = "getTitle";
    public $getContentFunctions = ['getContent'];
    
    protected $file;

    public function __construct() {
        $this->translations = new ArrayCollection();
        $this->updatedAt = new \DateTime();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getPhotosParameters() {
        return [
            'photo' => [
                'uploadedFile' => $this->file,
                'filename' => $this->photo,
                'filenameSetter' => 'setPhoto',
                'convertableToWebp' => true,
                'thumbs' => [
                    'full' => [
                        'width' => 1920,
                        'height' => 250
                    ]
                ]
            ]
        ];
    }
    
    protected function getUploadDir() {
        return 'uploads/blog';
    }
    
    public function setUpdatedAt(\DateTime $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt() : ?\DateTime
    {
        return $this->updatedAt;
    }
    
    public function getTranslation(string $locale) {
        $translation = null;
        foreach ($this->translations as $item) {
            if (strcmp($item->getLocale(), $locale) == 0) {
                $translation = $item;
            }
        }
        return $translation;
    }

    public function getTranslations() {
        return $this->translations;
    }

    public function addTranslation(BlogSettingsTranslation $translation) : self {
        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
            $translation->setObject($this);
        }

        return $this;
    }

    public function removeTranslation(BlogSettingsTranslation $translation) : self {
        $this->translations->removeElement($translation);
        
        return $this;
    }
    
    public function toString($locale = 'en') : string {
        if (is_null($this->getTranslation($locale))) {
            return 'New';
        } else {
            if (is_null($this->getTranslation($locale)->getTitle())) {
                return 'New';
            }
            return $this->getTranslation($locale)->getTitle();
        }
    }
    
    public function setPhoto(string $photo) : self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getPhoto() : ?string
    {
        return $this->photo;
    }
    
    public function setItemsLimit(int $itemsLimit) : self
    {
        $this->itemsLimit = $itemsLimit;

        return $this;
    }

    public function getItemsLimit() : ?int
    {
        return $this->itemsLimit;
    }
    
    public function setFile(UploadedFile $file = null) : void
    {
        $this->file = $file;
    }

    public function getFile() : ?UploadedFile
    {
        return $this->file;
    }
    
    public function setContentPosition(string $contentPosition) : self
    {
        $this->contentPosition = $contentPosition;

        return $this;
    }

    public function getContentPosition() : ?string
    {
        return $this->contentPosition;
    }
}
<?php

    
/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\BlogBundle\Entity\Blog;

class BlogRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Blog::class);
    }
    
    public function findAllSuitable($locale = null, $limit = null) {
        $today = new \DateTime();

        $qb = $this->createQueryBuilder('p');
            $qb->leftJoin('p.translations', 't');
            $qb->where('p.published = :published');
            $qb->andWhere('p.publishedAt <= :publishedAt');
            
            if ($locale != null) {
                $qb->andWhere('t.locale = :locale');
                $qb->andWhere('t.title is NOT NULL');
                $qb->andWhere('t.slug is NOT NULL');
                $qb->setParameter('locale', $locale);
            }
            
            if ($limit != null) {
                $qb->setMaxResults($limit);
            }
            
            $qb->setParameter('published', true);
            $qb->setParameter('publishedAt', $today);
            $qb->orderBy('p.publishedAt', 'desc');
        $query = $qb->getQuery();
        $result = $query->getResult();
        
        return $result;
    }
    
    public function findOneBySlug($slug, $locale) {
        $result = $this->createQueryBuilder('p')
            ->leftJoin('p.translations', 't')
            ->where('t.slug = :slug')
            ->andWhere('t.locale = :locale')
            ->andWhere('t.title is NOT NULL')
            ->andWhere('t.slug is NOT NULL')
            ->setParameter('slug', $slug)
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getOneOrNullResult();
        
        return $result;
    }
    
    public function findMenuParameters($defaultLocale = 'pl') {
        $entities = $this->createQueryBuilder('p')
            ->leftJoin('p.translations', 't')
            ->getQuery()
            ->getResult();
        
        $items = [];
        
        foreach ($entities as $entity) {
            $item = [];
            if ($entity->getTranslation($defaultLocale) != null) {
                $item['title'] = $entity->getTranslation($defaultLocale)->getTitle();
            } else {
                foreach ($entity->getTranslations() as $translation) {
                    $item['title'] = $entity->getTranslation($translation->getLocale())->getTitle();
                }
            }
            
            foreach ($entity->getTranslations() as $translation) {
                $item['translations'][] = [
                    'entityName' => Blog::class,
                    'entityId' => $entity->getId(),
                    'locale' => $translation->getLocale(),
                    'label' => 'slug',
                    'value' => $translation->getSlug(),
                ];
            }
            
            $items[] = $item;
        }
        
        return $items;
    }
}
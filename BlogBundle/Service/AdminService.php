<?php

/**
 * @author Oleh Vaskiv <contact@vaskiv.dev>
 * @link https://vaskiv.dev
 * @copyright (c) 2021, Oleh Vaskiv
 */
    
namespace App\BlogBundle\Service;
    
use App\UserBundle\Tools\AdminRightsBlock;
use App\UserBundle\Tools\AdminRightsBundle;
use App\UserBundle\Tools\AdminRightsRoute;
use App\BlogBundle\Entity\Blog;
    
class AdminService {
    private $container;

    public function __construct($container = null) {
        $this->container = $container;
    }
    
    public function addToMenu() {
        return [
            'title' => 'Blog',
            'route' => 'admin_blog',
            'icon' => '<i class="fas fa-blog"></i>'
        ];
    }
    
    public function addToUserGroup(AdminRightsBlock $parent) {
        $bundle = new AdminRightsBundle("Blog");
        $parent->addBundle($bundle);
        
        $bundle->addRoute(new AdminRightsRoute("List", 'admin_blog'));
        $bundle->addRoute(new AdminRightsRoute("Add", 'admin_blog_new'));
        $bundle->addRoute(new AdminRightsRoute("Edit", 'admin_blog_edit, admin_blog_crop, admin_blog_crop_save'));
        $bundle->addRoute(new AdminRightsRoute("Remove", 'admin_blog_delete, admin_blog_selected, admin_blog_selected_execute'));
        $bundle->addRoute(new AdminRightsRoute("Settings", 'admin_blog_settings, admin_blog_settings_crop, admin_blog_settings_crop_save'));
    }
    
    public function getFrontMenuOptions() {
        return [
            ['label' => 'Blog', 'route' => 'blog'],
            ['label' => 'Post', 'route' => 'blog_show', 'repositoryEntity' => Blog::class]
        ];
    }
}